/**
 * 1. Create the following array called "row"
 *  [10, 20, 30, 40, 50]
 * 2. Create an new array called "matrix" that:
 *  - maps over "row" and return each item, but in an array
 *  - (we should have a 2-dimensional array as a result)
 * 3. Print out both arrays
 * 
 * "matrix" should look like this:
 *    [[10], [20], [30], [40], [50]]
 */

const row = [10, 20, 30, 40, 50]

const matrix = row.map((r, i) => [r])

console.log(matrix)