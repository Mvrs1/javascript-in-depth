/**
 * 1. Create an array called 'bools' of the following booleans:
 *    [true, true, false, true, false, false]
 * 2. Map over 'bools' and do the following:
 *    - if "true", return a random number in it's price
 *    - if "false", return 0
 * 3. Print out both arrays
 * 
 * HINT: Google for 'random number js' to find a random number function
 * built-in to JavaScript to use 
 * 
 */

const bools = [true, true, false, true, false, false]

const ans = bools.slice().map(a => {
  let price = +a // simple way to convert bool to number true => 1 false => 0
  const b = []
  if (a === true) {
    price = Math.random() % price / bools.length
    b.push(price)
  } else {
    b.push(0)
  }
  return b
})

// correct approach

const mappedBools = bools.slice().map(bool => {
  if (bool) {
    return Math.random()
  }
  return 0
})

console.log(mappedBools)

console.log(ans)