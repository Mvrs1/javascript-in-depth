/**
 * 1. Create the following array called "nums"
 *    [10, 30, 50, 70, 90]
 * 2. Create a new array called squares that:
 *    - maps over 'nums' and return each item squared (x by itself)
 * 3. Create another new array called 'over1000' that:
 *    - filters 'squares' to contain only values over 1000
 * 4. Create a const called 'finale' that:
 *    - reduces 'over1000' to a single sum of it's elements
 * 
 * 5. Print out 'nums', 'squares', 'over1000' and finale
 */

// @ts-ignore
const nums = [10, 30, 50, 70, 90]

console.log(nums)

// @ts-ignore
const ans = nums.map((num) => num ** 2)
  .filter(num => num > 1000)
  .reduce((sum, num) => sum + num, 0)
console.log(ans)
