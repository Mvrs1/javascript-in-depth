const arr_nums = [5, 10, 15, 20, 25, 30, 35]

let sum = 0

for (let i = 0; i < arr_nums.length; i++) {
  sum += arr_nums[i]
}

console.log(sum)

const sum2 = arr_nums.reduce((prev, cur) => {
  if (cur < 20) {
    return prev + cur
  }
  return prev
}, 0)

console.log(sum2)

// reduce-string.js
const cities = ["Vacouver", "Berlin", "Seattle", "Toronto"]

const concatCities = cities.reduce((concatStrings, city) => {
  return concatStrings + city + "-"
}, "")

console.log(concatCities.slice(0, -1))

// array approach
const reduced = arr_nums.reduce((result, num) => {
  result.push(num)
  return result
}, [])

console.log(reduced)