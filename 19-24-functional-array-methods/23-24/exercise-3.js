/**
 * 1. Create the following array called 'items'
 *    ['light', 'banana', 'phone', 'book', 'mouse']
 * 2. Create an new array called 'caps' that:
 *    - maps over 'items' and capitalizes each item
 * 3. Create a const called 'concat' that:
 *    - uses reduce to concatenate all the strings in 'caps'
 *    using a space to seperate each item
 * 4. Print out 'items', 'caps' and 'concat'
 * 
 * BONUS: Can you do steps 1-3 in one line?
 */

// My solution only capitalizes the beginning of every item in the list

const caps = ['light', 'banana', 'phone', 'book', 'mouse']
  .map((item, idx) => item.replace(item[0].toLowerCase(), item[0].toUpperCase()))
  .reduce((result, item) => result + item + ' ', '')

console.log(caps)
