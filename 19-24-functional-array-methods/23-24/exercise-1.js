/**
 * 1. Create an array called 'companies' of the following steps
 *    ['apple', 'tesla', 'spacex', 'amazon', 'meta', 'google']
 * 
 * 2. Create a const called 'modded' that reduces the 'companies'
 *    array to a string of companies that DO NOT start with the letter 'a', 
 *    seperated by dashes
 * 3. Print out 'companies' and 'modded'
 */

const removeTrailChar = str => str.slice(0, -1)

const companies = ['apple', 'tesla', 'spacex', 'amazon', 'meta', 'google']
const modded = companies.reduce((result, company, i) => {
  if (company.startsWith('a')) {
    return result
  }

  if (i === companies.length - 1) {
    return result + company
  }

  return result + company + '-'
}, '')

// console.log(removeTrailChar(modded))

console.log(modded)