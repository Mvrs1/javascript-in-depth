/**
 * 1. Create an array called "practice" with the numbers 10-20 (inclusive)
 * 2. Create another array using filter that keeps the even numbers in "practicee"
 * 3. Print out both "practice" and the new filtered array
 */

const practice_1 = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
const evens = practice_1.filter(p => p % 2 === 0)

console.log(evens)
console.log(practice_1)


