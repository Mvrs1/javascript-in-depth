/**
 * 
 * 1. Create an array called 'prices' with the following values:
 *    [1.23, 19.99, 85.2, 32.87, 8, 5.2]
 * 2. Create a new array using filter called 'lowPrices' but keeps all the prices where the price
 *    plus a 15% tax is less than 10.00
 * 3. Print out both arrays
 */

const _prices = [1.23, 19.99, 85.2, 32.87, 8, 5.2]
const _tax = 1.15
const _lowPrices = _prices.filter(p => p * _tax < 10.00)

console.log(_lowPrices)
console.log(_prices)