/**
 * 1. Create the following array called "nums"
 *    [10, 20, 30, 40, 50]
 * 
 * 2. Create an new array called "timesTwo" that: 
 *    - maps over "nums" and returns each number multiplied by two
 * 3. Create an new array called "over50" that:
 *    - filters "timesTwo" to keep only values over 50
 * 
 * 4. Print out all 3 arrays
 * 
 * "over50" should contain: [60, 80, 100]
 */

const _nums = [10, 20, 30, 40, 50]
const timesTwo = _nums.map(n => n * 2)
console.log(timesTwo)

const over50 = timesTwo.filter(v => v > 50)

console.log(over50)

// ** chain solution
const _ans = [10, 20, 30, 40, 50]
  .map(v => v * 2)
  .filter(k => k > 50)

console.log(_ans)