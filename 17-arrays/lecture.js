const veggie1 = 'cucumber';
const veggie2 = 'tomato';
const veggie3 = 'onion';

console.log(veggie1, veggie2, veggie3);

const animals = ['dog', 'cat', 'bird', 'fish'];

const dog = animals[0];
console.log(dog);

const bird = animals[2];
console.log(bird);

console.log("-----------------")

for (let i = 0; i < animals.length; i++) {
  console.log(animals[i]);
}


animals.pop()

console.log(animals)

animals.unshift('monkey')

console.log(animals) // now its monkey, dog, cat, bird











