/**
 * 
 * 1. Create an array with the following strings:
 *    - "Mercury"
 *    - "Venus"
 *    - "Earth"
 *    - "Mars"
 * 
 * 2. Add the string "Jupiter" to the end of the array
 * 3. Add the string "Sun" to the START of the array
 * 4. Remove the last two strings from the array
 * 5. Remove the first two strings from the array
 * 
 * * Print out the array after each of these steps
 */

const planets = ['Mercury', 'Venus', 'Earth', 'Mars'];
const jupiter = 'Jupiter';
const sun = 'Sun'
planets.push(jupiter); // end of array
planets.unshift(sun) // start of array

planets.splice(-2) // remove last two strings
planets.splice(0, 2) // remove first two strings


console.log(planets);