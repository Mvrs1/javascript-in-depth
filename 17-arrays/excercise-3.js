/**
 * Create a variable called 'total' that starts at 0
 * 
 * Create an array called "grades" with the following values:
 *   - 55
 *   - 63
 *   - 82
 *   - 98
 *   - 91
 *   - 43
 * 
 * Figure out how to print out the AVERAGE grade
 * (This is the sum of all grades divided by the number of grades)
 */

let total = 0

const grades = [55, 63, 82, 98, 91, 43]
const len = grades.length

for (let i = 0; i < len; i++) {
  total += grades[i]
}

const average = total / len

console.log(average) // 72 is the answer 
